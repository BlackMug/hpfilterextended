import logging
import inspect
import datetime
import sys
import os
from functools import wraps


class extlog:
    function_debug = False

    _shift = 0
    _delta_shift = 4
    _file_handler = None
    _console_handler = None

    CRITICAL = 50
    FATAL = CRITICAL
    ERROR = 40
    WARNING = 30
    WARN = WARNING
    INFO = 20
    DEBUG = 10
    NOTSET = 0

    def __init__(self):
        self.init_log()

    @staticmethod
    def error(message):
        filepath, linenum, funcname = extlog.get_frame_info()
        logging.error(filepath + ':' + str(linenum) + ' (' + funcname + '): ' + message)

    @staticmethod
    def debug(message):
        logging.debug(message)

    @staticmethod
    def info(message):
        logging.info(message)

    @staticmethod
    def shift_messages(to_right=True):
        if to_right:
            extlog._shift += extlog._delta_shift
        else:
            extlog._shift -= extlog._delta_shift
        log_formatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]: {0}%(message)s".format(" " * extlog._shift))
        extlog._file_handler.setFormatter(log_formatter)
        extlog._console_handler.setFormatter(log_formatter)

    @staticmethod
    def get_frame_info(level=2):
        frame = inspect.getframeinfo(inspect.getouterframes(inspect.currentframe())[level].frame)
        return frame.filename, frame.lineno, frame.function

    @staticmethod
    def get_func_info(func):
        return func.__name__, func.__module__

    @staticmethod
    def enter_func(func=None):
        message = "-> from  %s:%d  calling  %s() =>  %s::%s" % (*extlog.get_frame_info(3), *extlog.get_func_info(func))
        logging.debug(message)
        extlog.shift_messages()

    @staticmethod
    def leave_func(func=None):
        message = "<- %s::%s" % extlog.get_func_info(func)
        extlog.shift_messages(False)
        logging.debug(message)

    @staticmethod
    def is_inited():
        return extlog._console_handler is not None \
               and extlog._file_handler is not None

    @staticmethod
    def init_log(out_log_folder=u'log', out_log_file=u'log.txt'):
        print("Initialize logging...")

        log_formatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]: %(message)s")
        root_logger = logging.getLogger()
        root_logger.setLevel(logging.DEBUG)

        out_log_file = str(datetime.datetime.now()).replace(':', '.') + '_' + out_log_file
        file_path = out_log_file
        if out_log_folder:
            if not os.path.exists(out_log_folder):
                os.makedirs(out_log_folder)
            file_path = out_log_folder + '/' + out_log_file
        extlog._file_handler = logging.FileHandler(file_path, mode="a")
        extlog._file_handler.setFormatter(log_formatter)
        root_logger.addHandler(extlog._file_handler)

        extlog._console_handler = logging.StreamHandler(sys.stdout)
        extlog._console_handler.setFormatter(log_formatter)
        root_logger.addHandler(extlog._console_handler)

        print("Initialize logging complete.")

    @staticmethod
    def set_level(level):
        # Requires logging level from logging library (For example, logging.ERROR)

        extlog._file_handler.setLevel(level)
        extlog._console_handler.setLevel(level)

    @staticmethod
    def logme(func):
        if not extlog.function_debug:
            return func
        else:
            @wraps(func)
            def wrapper(*args, **kwargs):
                extlog.enter_func(func)

                # # Cut extra variables for functions (for signals and slots)
                # argc = len(inspect.signature(func).parameters)
                # args = args[:argc]

                ret = func(*args, **kwargs)
                extlog.leave_func(func)
                return ret
        return wrapper
