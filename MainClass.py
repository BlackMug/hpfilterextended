import configparser
import os

import pyqtgraph as pg
from PyQt5 import uic
from PyQt5.QtCore import pyqtSlot, QObject, Qt, QPointF
from PyQt5.QtWidgets import QSpacerItem

from CustomQtWidgets import ClickableViewBox, MainWindow
from EventLog import EventLog
from PlotterHandler import PlotterHandler, Player, DPHHodrickPrescott, SelectionMode
from mt4reader import *


@extlog.logme
def set_enabled_to_layout_widgets(layout, is_enabled):
    for i in range(layout.count()):
        widget = layout.itemAt(i).widget()
        if widget:
            widget.setEnabled(is_enabled)
        else:
            if not type(layout.itemAt(i)) == QSpacerItem:
                set_enabled_to_layout_widgets(layout.itemAt(i), is_enabled)


class StatusHandler(QObject):
    _status_list = None  # QListWidget
    _params_items = {}

    @extlog.logme
    def __init__(self, status_list, parent=None):
        super().__init__(parent=parent)
        if status_list is None:
            raise ValueError("status_list should be not None")
        self._status_list = status_list

    @pyqtSlot(dict)
    def update_direction_trend(self, params):
        try:
            name = params['name']
            key = name+'_item'

            if key not in self._params_items:
                self._params_items[key] = self._status_list.addItem(name+" trend")

            item = self._params_items[key]
            item.setText(params['text'])
            item.setBackground(params.get('color', Qt.white))
        except KeyError as e:
            extlog.error("Key {} not found in dictionary".format(e.args[0]))


class MainClass(QObject):

    # Constants
    _data_path = u"data"
    _data_file_ext = u".hst"
    _settings_filename = "settings.ini"
    _settings_section = "User"

    # Variables
    _ui = None
    _plot_field = None
    _plotter_handler = None
    _status_handler = None
    _player = None
    _view_box = None

    _event_log = None

    _current_file = None

    _app = None

    @extlog.logme
    def __init__(self, ui_file, app, parent=None):
        super().__init__(parent=parent)
        self._app = app
        if (ui_file is None) or (not os.path.exists(ui_file)):
            error_message = "Ui file {0} not initialized or not exist.".format(str(ui_file))
            extlog.error(error_message)
            raise FileNotFoundError(error_message)

        class TimeAxisItem(pg.AxisItem):
            def tickStrings(self, values, scale, spacing):
                return [datetime.datetime.fromtimestamp(value) if value >= 86400 else "" for value in values]

        self._view_box = ClickableViewBox()

        date_axis = TimeAxisItem(orientation='bottom')
        self._plot_field = pg.PlotWidget(viewBox=self._view_box, axisItems={'bottom':date_axis})
        self._player = Player()
        self._plotter_handler = PlotterHandler(self.get_plotter())

        self.init_ui(ui_file)

        # TODO: use this
        # self._status_handler = StatusHandler(self._ui.status_list_common)
        self._plotter_handler.set_to_all_status_handler(self._status_handler)

        self.init_signals_and_slots()

        if not self.load_settings():
            extlog.info('Unable to read settings from "%s". Default values will be used.' % self._settings_filename)

        # Pick file for plotting
        self.switch_to_data_file(self._ui.in_file_name_combobox.currentText())

    def get_plotter(self):
        return self._plot_field.getPlotItem()

    @extlog.logme
    def fill_data_file_combobox(self):
        filenames = [self._data_path + "\\" + filename
                     for filename in os.listdir(self._data_path) if filename.endswith(self._data_file_ext)]
        self._ui.in_file_name_combobox.clear()
        self._ui.in_file_name_combobox.addItems(filenames)

    @pyqtSlot()
    @extlog.logme
    def toggle_drawing(self):
        # Constants
        disable_drawing = 'Disable drawing'
        enable_drawing = 'Enable drawing'

        is_enabled = self._plotter_handler.toggle_drawing_enabled()
        if is_enabled:
            self._ui.toggle_drawing_button.setText(disable_drawing)
        else:
            self._ui.toggle_drawing_button.setText(enable_drawing)

        self._ui.reset_view_button.setEnabled(is_enabled)
        self._ui.adapt_button.setEnabled(is_enabled)

        set_enabled_to_layout_widgets(self._ui.player_controls_group, is_enabled)
        self._ui.play_button.setEnabled(True)
        self._ui.reset_player_button.setEnabled(True)

    @extlog.logme
    def init_ui(self, ui_file):
        self._ui = uic.loadUi(uifile=ui_file)
        self._ui.plot_layout.addWidget(self._plot_field)

        # Set values
        self.fill_data_file_combobox()

        self._ui.hp_lambda_value_spinbox.setValue(DPHHodrickPrescott.default_la)
        self._ui.view_radius_spinbox.setValue(PlotterHandler.default_view_radius)
        self._ui.filter_deep_spinbox.setValue(DPHHodrickPrescott.default_filter_deep)
        self._ui.speed_spinbox.setValue(Player.default_speed)
        self._ui.speed_slider.setValue(Player.default_speed)

        self._event_log = EventLog.get_event_log(self._ui.event_log, self)

        self._ui.show()

    @extlog.logme
    def load_settings(self):
        if not os.path.exists(self._settings_filename):
            return False

        config = configparser.ConfigParser()
        config.read(self._settings_filename)

        section = self._settings_section
        if section not in config:
            return False

        try:
            data_file = str(config[section]['data_file'])
            file_index = self._ui.in_file_name_combobox.findText(data_file)
            if file_index != -1:
                self._ui.in_file_name_combobox.setCurrentIndex(file_index)

            self._ui.hp_lambda_value_spinbox.setValue(int(config[section]['lambda']))
            self._ui.filter_deep_spinbox.setValue(int(config[section]['filter_deep']))
            self._ui.speed_spinbox.setValue(int(config[section]['speed']))
            self._ui.speed_slider.setValue(int(config[section]['speed']))
            self._ui.view_radius_spinbox.setValue(int(config[section]['view_radius']))
            self._ui.extra_size_spinbox.setValue(int(config[section]['extra_size']))

            extlog.info('Successfully read settings from "%s".' % self._settings_filename)
        except Exception as e:
            import traceback
            import sys
            traceback.print_tb(sys.exc_info()[2])
            extlog.error(str(type(e)) + ": " + e.__str__())
            return False

        return True

    @extlog.logme
    def save_settings(self):
        section = self._settings_section
        config = configparser.ConfigParser()
        config[section] = {}
        config[section]['lambda'] = str(self._ui.hp_lambda_value_spinbox.value())
        config[section]['filter_deep'] = str(self._ui.filter_deep_spinbox.value())
        config[section]['data_file'] = str(self._ui.in_file_name_combobox.currentText())
        config[section]['speed'] = str(self._ui.speed_spinbox.value())
        config[section]['view_radius'] = str(self._ui.view_radius_spinbox.value())
        config[section]['extra_size'] = str(self._ui.extra_size_spinbox.value())

        with open(self._settings_filename, 'w') as settings_file:
            config.write(settings_file)

        extlog.info('Settings written to "%s".' % self._settings_filename)

    @extlog.logme
    def init_signals_and_slots(self):
        # Settings
        self._ui.in_file_name_combobox.currentIndexChanged[str].connect(self.switch_to_data_file)
        self._ui.view_radius_spinbox.valueChanged[int].connect(self._plotter_handler.set_view_radius)
        self._ui.filter_deep_spinbox.valueChanged[int].connect(self._plotter_handler.set_to_all_filter_deep)
        self._ui.hp_lambda_value_spinbox.valueChanged[int].connect(self._plotter_handler.set_to_all_lambda)
        self._ui.extra_size_spinbox.valueChanged[int].connect(self._plotter_handler.set_to_all_extra_size)

        self._ui.toggle_drawing_button.clicked.connect(self.toggle_drawing)
        self._ui.reset_view_button.clicked.connect(self._plotter_handler.adjust_view_box)
        self._ui.adapt_button.clicked.connect(self._plotter_handler.slot_enable_adapt_to_y)

        self._ui.selection_none_rbutton.toggled.connect(lambda is_toggled:
            is_toggled and self._plotter_handler.set_selection_mode(SelectionMode.NONE)
        )
        self._ui.selection_up_rbutton.toggled.connect(lambda is_toggled:
            is_toggled and self._plotter_handler.set_selection_mode(SelectionMode.UP)
        )
        self._ui.selection_horizontal_rbutton.toggled.connect(lambda is_toggled:
            is_toggled and self._plotter_handler.set_selection_mode(SelectionMode.HORIZONTAL)
        )
        self._ui.selection_down_rbutton.toggled.connect(lambda is_toggled:
            is_toggled and self._plotter_handler.set_selection_mode(SelectionMode.DOWN)
        )

        self._ui.write_history_button.clicked.connect(self._plotter_handler.write_all_history)

        # Position
        self._ui.playback_spinbox.valueChanged.connect(self._ui.playback_slider.setValue)
        self._ui.playback_slider.valueChanged.connect(self._ui.playback_spinbox.setValue)
        # Speed
        self._ui.speed_spinbox.valueChanged.connect(self._ui.speed_slider.setValue)
        self._ui.speed_slider.valueChanged.connect(self._ui.speed_spinbox.setValue)
        self._ui.speed_spinbox.valueChanged.connect(self._player.set_speed)

        # Interaction with plot widget
        self._view_box.click_here[QPointF, int].connect(self._plotter_handler.click_here)

        # Player controls
        self._ui.play_button.clicked.connect(self.toggle_play)
        self._ui.step_button.clicked.connect(self._player.need_next_step)
        self._ui.automove_checkbox.toggled.connect(self.set_automove)
        self._plotter_handler.plotter.getViewBox().sigRangeChangedManually\
            .connect(self.disable_automove_checkbox_slot)
        self._ui.reset_player_button.clicked.connect(self._player.reset)
        self._ui.reset_player_button.clicked.connect(self.force_pause)

        # Player signals
        self._player.progress_changed.connect(self._ui.playback_spinbox.setValue)
        self._player.need_reset_history.connect(self._plotter_handler.reset_to_all_history)
        self._player.first_tick_changed.connect(self._plotter_handler.set_first_tick)
        self._player.new_range_signal[int].connect(self._plotter_handler.set_range)

        # Settings saving on exit
        self._app.lastWindowClosed.connect(self.call_save_segments)
        self._app.lastWindowClosed.connect(self.save_settings)

        # MainWindow signals
        self._ui.key_pressed[int].connect(self.key_pressed)

    @extlog.logme
    def key_pressed(self, key):
        if key in [Qt.Key_Right, Qt.Key_D]:
            self._plotter_handler.move_view_box(5)
        elif key in [Qt.Key_Left, Qt.Key_A]:
            self._plotter_handler.move_view_box(-5)

    @extlog.logme
    def call_save_segments(self):
        self._plotter_handler.save_segments(self._current_file)

    @extlog.logme
    def force_pause(self):
        if self._player.isActive():
            self.toggle_play()

    @pyqtSlot()
    @extlog.logme
    def toggle_play(self):
        play_label = 'Play'
        pause_label = 'Pause'

        is_activate = not self._player.isActive()

        if is_activate:
            self._player.start(max_len=self._plotter_handler.get_last_orig_stamp())
            self._ui.automove_checkbox.setChecked(True)
        else:
            self._player.stop()

        self._ui.play_button.setText(pause_label if is_activate else play_label)
        set_enabled_to_layout_widgets(self._ui.common_settings_layout, not is_activate)
        set_enabled_to_layout_widgets(self._ui.filters_settings_layout, not is_activate)

    @pyqtSlot()
    @extlog.logme
    def disable_automove_checkbox_slot(self):
        self._ui.automove_checkbox.setChecked(False)

    @pyqtSlot(bool)
    @extlog.logme
    def set_automove(self, val):
        self._plotter_handler.set_automove_view(val)
        if val is True:
            self._player.need_move_signal.connect(self._plotter_handler.adjust_view_box)
        else:
            self._player.need_move_signal.disconnect(self._plotter_handler.adjust_view_box)

    @extlog.logme
    def switch_to_data_file(self, data_file):
        extlog.info("Switching to file {}".format(data_file))

        self._ui.reset_player_button.click()
        data_reader = mt4reader(data_file)
        data_reader.read_data()
        open_data = mt4reader.get_open_data(data_reader.get_data)
        self._plotter_handler.set_timeframe(data_reader.get_timeframe)
        self._plotter_handler.set_series(open_data, file_label=data_file)
        self._plotter_handler.adjust_view_box()

        if self._current_file != data_file:
            if self._current_file is not None :
                self.call_save_segments()
            self._plotter_handler.load_segments(data_file)

        self._current_file = data_file

