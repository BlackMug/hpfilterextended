from PyQt5.QtCore import QObject, QAbstractTableModel, QModelIndex, QVariant, Qt, pyqtSlot
from PyQt5.QtWidgets import QHeaderView

from ext_logging import extlog


class Actions:

    def open_folder_with_file(filepath):
        from PyQt5.QtWidgets import QMessageBox
        import platform
        if platform.system() != 'Windows':
            QMessageBox.warning(None, 'Error', 'Unable to open file manager on your OS.')

        import subprocess
        subprocess.Popen(r'explorer /select,"%s"'%filepath)


class MyTableModel(QAbstractTableModel):

    _events = []

    def __init__(self, parent=None):
        super(MyTableModel, self).__init__(parent=parent)

    @extlog.logme
    def log_event(self, data_in):
        extlog.info("New event: " + str(data_in))

        self.beginInsertRows(QModelIndex(), 0, 0)
        self.append_event(data_in)
        self.endInsertRows()

    @extlog.logme
    def rowCount(self, parent=QModelIndex(), *args, **kwargs):
        return len(self._events)

    @extlog.logme
    def columnCount(self, parent=QModelIndex(), *args, **kwargs):
        return len(self._events[0]) if self.rowCount() else 0

    @extlog.logme
    def data(self, index, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            return self._events[index.row()][index.column()]
        else:
            return QVariant()

    def flags(self, index):
        return Qt.ItemIsEnabled | Qt.ItemIsSelectable

    # Own methods

    def append_event(self, event):
        self._events.insert(0, event)


class EventLog(QObject):
    _view = None
    _model = None

    # Singleton variable
    unique_event_log = None

    @staticmethod
    def get_event_log(table_view=None, parent=None):
        # if not EventLog.unique_event_log and not table_view:
        is_able_to_return = EventLog.unique_event_log or table_view
        assert is_able_to_return, ('First call of get_event_log. '
                                   'Unable to construct EventLog with table_view={0}'.format(table_view))

        if not EventLog.unique_event_log:
            EventLog.unique_event_log = EventLog(table_view, parent)

        return EventLog.unique_event_log

    @extlog.logme
    def double_click_elem(self, index):
        row = index.row()
        message = self._model.data(index.sibling(row, 1))
        substr = 'file: '
        if substr in message:
            filepath = message.split(substr)[1]
            Actions.open_folder_with_file(filepath)

    @extlog.logme
    def log_event(self, event_type, event):
        self._model.log_event([event_type, event])

    @extlog.logme
    def __init__(self, table_view, parent=None):
        super().__init__(parent=parent)
        self._view = table_view
        self._model = MyTableModel()
        self._view.setModel(self._model)

        self._view.doubleClicked.connect(self.double_click_elem)
        self._view.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

        self.log_event('Info', 'Event logger inited')  # Just because without it not works ?_?. TODO: fix this
