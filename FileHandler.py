import csv

import os
from enum import Enum

from EventLog import EventLog
from ext_logging import extlog
from MathExt import calc_correlation
from datetime import datetime
import pickle


class FileColumns:
    # x = 'X value'
    x_time = 'X timed value'
    # tg_val = 'Tangent value'
    norm_orig_val = 'Normalized orig value'
    norm_filtered_orig_val = 'Normalized filtered orig value'
    norm_filtered_last_val = 'Normalized filtered last value'
    norm_error_val = 'Normalized error value'
    count_predicted = 'Number of predicted ticks'
    # zigzag_tg_val = 'ZigZag tangent value'

    val_list = None

    @staticmethod
    def values():
        if FileColumns.val_list is None:
            FileColumns.val_list = FileColumns.items().values()
        return FileColumns.val_list

    @staticmethod
    def items():
        attrs = dict(FileColumns.__dict__.items())
        for exclude in ['__module__', '__dict__', '__weakref__', '__doc__', 'values', 'items', 'val_list']:
            attrs.pop(exclude)
        return attrs


@extlog.logme
def write_structure_to_file(data, filename):
    try:
        with open(filename, 'wb') as f:
            pickle.dump(data, f)
    except Exception as e:
        extlog.error("Unable to write structure to file {}: ({}) {}".format(filename, type(e), e.__str__()))


@extlog.logme
def read_structure_from_file(filename):
    try:
        with open(filename, 'rb') as f:
            return pickle.load(f)
    except Exception as e:
        extlog.error("Error while reading structure from file {}: ({}) {}".format(filename, type(e), e.__str__()))

    return {}


def write_cvs_head(detail_history_data, extra_size, file_label, min_max, name_suffix, writer):
    writer.writerow(['File label: ', file_label])
    writer.writerow(['Fileter name: ', name_suffix])
    writer.writerow(['Minimum: ', min_max[0], 'Maximum:', min_max[1]])
    writer.writerow(['Extra_size: ', extra_size])
    writer.writerow(['n: ', len(list(detail_history_data.values())[0])])
    # writer.writerow(['rXY: ', calc_correlation(
    #     [detail_history_data[FileColumns.norm_orig_val][i] - detail_history_data[FileColumns.norm_filtered_val][i] \
    #      for i in range(len(detail_history_data[FileColumns.norm_orig_val]))],
    #     detail_history_data[FileColumns.zigzag_tg_val]
    # )])
    # writer.writerow(['rXY (abs err): ', calc_correlation(
    #     detail_history_data[FileColumns.norm_error_val],
    #     [abs(v) for v in detail_history_data[FileColumns.zigzag_tg_val]]
    # )])
    writer.writerow(detail_history_data.keys())


@extlog.logme
def write_segment_analysis_to_file(file_label, name_suffix, detail_history_data, min_max, extra_size, segments_dict, la):
    count = len(list(detail_history_data.values())[0])
    if count == 0:
        return

    if segments_dict is None:
        return

    foldername = 'history'

    for direction, segments in segments_dict.items():
        if not bool(segments):
            continue

        filename = 'analysis_{}_{}_steps_segments_{}_la{}_n{}.csv'.format(name_suffix, extra_size, str(direction), la, count)\
            .replace(' ', '_')
        filepath = os.path.join(foldername, filename)

        if not os.path.exists(foldername):
            os.mkdir(foldername)

        try:
            with open(filepath, 'w', newline='') as csv_file:
                writer = csv.writer(csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                write_cvs_head(detail_history_data, extra_size, file_label, min_max, name_suffix, writer)

                data_lists = list(detail_history_data.values())
                it = iter(sorted(segments))
                segment = next(it)

                x = detail_history_data[FileColumns.x_time]

                for i in range(len(x)):
                    if x[i] > segment[1]:
                        try:
                            segment = next(it)
                        except Exception as e:
                            extlog.info("Seems like segments are ended.")
                            break

                    if segment[0] <= x[i]:
                        try:
                            writer.writerow([str(datetime.fromtimestamp(l[i])) if l == x else l[i] for l in data_lists])
                        except Exception as e:
                            extlog.error("Error while creating tuple for write to log (segment={}): {}"
                                         .format(segment, e.__str__()))

            EventLog.get_event_log().log_event('Info', 'History written to file: %s' % filepath)
        except Exception as e:
            extlog.error("Error while writing '{}' log: ({}) {}".format(filename, type(e), e.__str__()))


@extlog.logme
def write_analysis_to_file(file_label, name_suffix, detail_history_data, min_max, extra_size, la):
    count = len(list(detail_history_data.values())[0])
    if count == 0:
        extlog.info("Nothing analysis data to write. Exiting.")
        return

    filename = 'analysis_%s_%d_steps_la%d_n%d.csv' % (name_suffix, extra_size, la, count)
    foldername = 'history'
    filepath = os.path.join(foldername, filename)

    if not os.path.exists(foldername):
        os.mkdir(foldername)

    try:
        with open(filepath, 'w', newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            write_cvs_head(detail_history_data, extra_size, file_label, min_max, name_suffix, writer)

            x = [str(datetime.fromtimestamp(x)) for x in detail_history_data[FileColumns.x_time]]

            val_iters = [iter(val) for val in detail_history_data.values()]
            val_iters[0] = iter(x)
            for i in range(count):
                try:
                    writer.writerow([next(it) for it in val_iters])
                except Exception as e:
                    extlog.error("Error while creating tuple for write to log (i={}): {}".format(i, e.__str__()))

        EventLog.get_event_log().log_event('Info', 'History written to file: %s' % filepath)
    except Exception as e:
        extlog.error("Error while writing '{}' log: ({}) {}".format(filename, type(e), e.__str__()))
