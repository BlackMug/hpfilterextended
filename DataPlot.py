import math

from FileHandler import *
import MathExt
from ext_logging import extlog
from PyQt5.QtGui import QColor
import pyqtgraph
import numpy as np


@extlog.logme
def get_range_bounds(range_):
    return range_[0], range_[-1]+1


@extlog.logme
def extend_range_to_right(range_to_extend, ext_value):
    (start, end) = get_range_bounds(range_to_extend)
    return range(start, end+ext_value)


def extend_time_range_to_right(range_to_extend, extra_size, step):
    if step is None:
        step = range_to_extend[-1]-range_to_extend[-2]

    for i in range(extra_size):
        range_to_extend.append(range_to_extend[-1]+step)

    return range_to_extend


class Direction(Enum):
    DOWN = -1
    UP = 1
    NONE = None

    def __str__(self):
        str = 'None'
        if self == Direction.UP:
            str = 'UP'
        if self == Direction.DOWN:
            str = 'DOWN'
        return str


class ColorShuffler:

    class ColorListItem:
        color = None
        used = False

        def __init__(self, color):
            self.color = color

    colors = [
        ColorListItem(QColor(139, 139, 131)),
        ColorListItem(QColor(255, 0, 0)),
        ColorListItem(QColor(0, 191, 255)),
        ColorListItem(QColor(255, 255, 0)),
        ColorListItem(QColor(152, 251, 152)),
        ColorListItem(QColor(186, 85, 211)),
        ColorListItem(QColor(255, 165, 0)),
        ColorListItem(QColor(255, 215, 0)),
        ColorListItem(QColor(124, 252, 0)),
        ColorListItem(QColor(127, 255, 212)),
        ColorListItem(QColor(0, 0, 255)),
        ColorListItem(QColor(65, 105, 255)),
        ColorListItem(QColor(200, 200, 200)),
        ColorListItem(QColor(255, 255, 255))
    ]

    @staticmethod
    def get_unique_color():
        # shuffle(ColorShuffler.colors)
        for color_item in ColorShuffler.colors:
            if not color_item.used:
                color_item.used = True
                return color_item.color


class ZigZagHandler:
    _depth = 12         # Depth
    _deviation = 5      # Deviation
    _backstep = 3       # Backstep

    _data = None        # Pointer to data
    _zigzag = None      # List with zigzag values

    _zigzag_tangents_data = None

    def __init__(self, in_depth=None, in_deviation=None, in_backstep=None, in_data=None):
        if in_depth:
            self._depth = in_depth
        if in_deviation:
            self._deviation = in_deviation
        if in_backstep:
            self._backstep = in_backstep

        if in_data is not None:
            self.calc_zigzag(in_data)

    def calc_zigzag(self, in_data):
        if self._data is None:
            self._data = in_data
        self._zigzag = MathExt.zigzag(self._depth, self._deviation, self._backstep, self._data)
        self._zigzag = MathExt.zigzag_simple(self._data, 100)
        self._zigzag_tangents_data = self.calc_zigzag_tangents(self.zigzag_data)

    def calc_zigzag_tangents(self, in_zigzag):
        items = list(in_zigzag.items())
        i = 0
        prev = items[i]
        i += 1
        cur = items[i]
        i += 1

        ret = []
        finish = items[-1][0]
        for j in range(finish):
            ret.append( (cur[1]-prev[1]) / (cur[0]-prev[0]) )
            if j == cur[0]:
                prev = cur
                if j != finish:
                    cur = items[i]
                    i += 1

        return ret

    @property
    def zigzag_data(self):
        return self._zigzag

    @extlog.logme
    @property
    def zigzag_tangents_data(self):
        return self._zigzag_tangents_data


class DataPlotHandler:
    # Class handles data in series
    # and keep its settings

    _history_file_label = None

    _history_data = None
    _history_plot_item = None
    _detail_history_data = None

    _orig_data = None           # read only, except update_data() use
    _x_values = None
    _data_plot_item = None

    _zigzag_enabled = False
    _zigzag_handler = None
    _zigzag_plot_item = None

    _need_rendering = True
    _color = None
    _plotter = None
    _timeframe = None

    _already_played = False  # Check for first update or not. Reset when history reset calls
    _cur_index = None

    _status_handler = None

    _minmax_y_ext_cache = None

    @extlog.logme
    def __init__(self, plot_widget, color=None):
        self._plotter = plot_widget
        self.set_color(color)

    @extlog.logme
    def set_color(self, color):
        if color is None:
            self._color = ColorShuffler.get_unique_color()
            extlog.info("Selected color for series: %s." % str(self._color.name()))
        else:
            self._color = color
            extlog.info("Set color for series: %s." % str(self._color.name()))

    @extlog.logme
    def set_timeframe(self, timeframe):
        self._timeframe = timeframe

    @extlog.logme
    def set_need_rendering(self, need_rendering):
        self._need_rendering = need_rendering

    @property
    def max_x(self):
        if not self._data_plot_item:
            return 0
        return self._data_plot_item.xData.max()

    @property
    def min_max_y_ext(self):
        if self._minmax_y_ext_cache is not None:
            return self._minmax_y_ext_cache

        min_max = self._orig_data.min(), self._orig_data.max()
        height = min_max[1]-min_max[0]
        if height < 0:
            extlog.error("Strange: min_max[1]={}-min_max[0]={} = height{} < 0".format(min_max[0], min_max[1], height))
            height = 0
        self._minmax_y_ext_cache = min_max[0]-height*0.2, min_max[1]+height*0.2
        return self._minmax_y_ext_cache

    @property
    def orig_data(self):
        return self._orig_data

    @extlog.logme
    def update_data(self, data=None, end_stamp=None, file_label=None):
        if data is not None:
            # Update orig data if new data received (first data receive maybe)
            x_values, self._orig_data = data
            self._x_values = [x.timestamp() for x in x_values]
            self._history_file_label = file_label

            if self._zigzag_enabled:
                self._zigzag_handler = ZigZagHandler(in_data=self.orig_data)

            self.reset_data()

        if self._orig_data is None:
            extlog.error("No data to update.")
            return

        self._cur_index = end_stamp-1 if end_stamp is not None else len(self.orig_data)-1
        self.set_plot_data(self._orig_data[:end_stamp], data_range_list=self._x_values[:end_stamp])

    @extlog.logme
    def reset_data(self):
        if self._zigzag_plot_item:
            self._zigzag_plot_item.clear()
            self._zigzag_plot_item = None
        if self._data_plot_item:
            self._data_plot_item.clear()
            self._data_plot_item = None
        if self._history_plot_item:
            self._history_plot_item.clear()
            self._history_plot_item = None
        if hasattr(self, '_perm_border_plot_item') and self._perm_border_plot_item is not None:
            self._plotter.removeItem(self._perm_border_plot_item)
            del self._perm_border_plot_item
        if self._minmax_y_ext_cache:
            self._minmax_y_ext_cache = None

    @extlog.logme
    def set_plot_data(self, plot_data, data_range_list=None):
        # Common functions for all DPH family

        if plot_data is None:
            extlog.error("No data set. %s() requires data." % __name__)
            return

        if data_range_list is None:
            data_range_list = self._x_values

        if hasattr(self, 'log_filter_error'):
            self.log_filter_error(data_range_list, plot_data)

        if self._need_rendering:
            # Visualization data
            try:
                extlog.debug('Refreshing data.')
                self._data_plot_item.setData(x=data_range_list, y=plot_data)
                if not (type(self) == DataPlotHandler):
                    self._history_plot_item.setData(x=list(self._history_data.keys()),
                                                    y=list(self._history_data.values()))
                if hasattr(self,'_perm_border_plot_item'):
                    from_= self._x_values[self._cur_index]
                    to_ = from_ + self._timeframe*100
                    fromto = range(int(from_), int(to_))
                    a, b = self._predict_line
                    self._perm_border_plot_item.setData(x=list(fromto), y=[a*x+b for x in fromto])

            except AttributeError:
                extlog.debug('Seems like no data set yet. Creating series data.')

                # PlotItem.plot() returns PlotDataItem
                self._data_plot_item = self._plotter.plot(x=data_range_list, y=plot_data,
                                                          pen=pyqtgraph.mkPen(self._color, width=1))
                if not (type(self) == DataPlotHandler):
                    self._history_plot_item = self._plotter.plot(x=list(self._history_data.keys()),
                                                                 y=list(self._history_data.values()),
                                                                 pen=pyqtgraph.mkPen(self._color.darker(250), width=1))

                    from_= self._x_values[self._cur_index]
                    to_ = from_ + self._timeframe*100
                    fromto = range(int(from_), int(to_))
                    a, b = self._predict_line
                    self._perm_border_plot_item = self._plotter.plot(x=list(fromto),
                                                                     y=[a*x+b for x in fromto],
                                                                     pen=pyqtgraph.mkPen(self._color.lighter(),
                                                                                         width=1))

                elif self._zigzag_enabled:
                    self._zigzag_plot_item = self._plotter.plot(x=list(self._zigzag_handler.zigzag_data.keys()),
                                                                y=list(self._zigzag_handler.zigzag_data.values()),
                                                                pen=pyqtgraph.mkPen(QColor(0, 193, 195), width=1))

    @extlog.logme
    def set_status_handler(self, status_handler):
        self._status_handler = status_handler


class DPHHodrickPrescott(DataPlotHandler):
    # Constants
    name_suffix = "HodrickPrescott"
    global_filtered_data_extension = '.gfd'

    # Variables
    default_la = 1600
    _la = None

    _default_extra_size = 0
    _extra_size = 0

    default_filter_deep = 1000
    _filter_deep = default_filter_deep

    _filtered_orig_data = None

    _perm_border_plot_item = None  # Permissible error frame for prediction
    _predict_line = (None, None)
    _predict_direction = Direction.NONE

    @extlog.logme
    def __init__(self, plot_widget, la=None, color=None):
        super(DPHHodrickPrescott, self).__init__(plot_widget, color)
        self.reset_history()
        self._la = la if la else self.default_la

    @property
    def extra_size(self):
        return self._extra_size

    @extlog.logme
    def set_la(self, la):
        self._la = la

    @extlog.logme
    def set_filter_deep(self, filter_range):
        self._filter_deep = filter_range

    @extlog.logme
    def get_filter_deep(self):
        return self._filter_deep

    @extlog.logme
    def write_history_to_file(self, segments):
        if self.orig_data is not None:
            write_analysis_to_file(self._history_file_label, self.name_suffix
                                   , self._detail_history_data
                                   , self.min_max_y_ext, self._extra_size, self._la)
            write_segment_analysis_to_file(self._history_file_label, self.name_suffix
                                           , self._detail_history_data
                                           , self.min_max_y_ext, self._extra_size, segments, self._la)

    @extlog.logme
    def reset_history(self):
        self._history_data = {}
        self._detail_history_data = {col_name: [] for col_name in FileColumns.values()}
        self._already_played = False

    @extlog.logme
    def construct_data_range(self, end_stamp):
        # Returns range of stamps (from-to)
        # Cuts from _filter_deep till end_stamp

        if not end_stamp:
            end_stamp = len(self._orig_data)

        data_range = range(end_stamp - self._filter_deep - 1, end_stamp) \
                           if end_stamp > self._filter_deep else \
                           range(end_stamp)

        return data_range

    @extlog.logme
    def apply_new_data(self, data, file_label=None):
        x_values, self._orig_data = data
        self._x_values = [x.timestamp() for x in x_values]

        la_suffix = 'la' + str(self._la)
        data_filename = file_label + la_suffix + DPHHodrickPrescott.global_filtered_data_extension
        if file_label is not None:
            try:
                self._filtered_orig_data = read_structure_from_file(data_filename)
                extlog.info("Filtered original data loaded from cache file ({}).".format(data_filename))
            except Exception as e:
                extlog.error("Error while reading pickle data from file '{}' log: ({}) {}".format(
                             file_label, type(e), e.__str__()))
        if file_label is None or\
                self._filtered_orig_data is None or \
                len(self._filtered_orig_data) != len(self._orig_data):
            self._filtered_orig_data = np.array(self._orig_data)
            MathExt.hodrick_prescott_filter(self._filtered_orig_data, self._la)
            try:
                write_structure_to_file(self._filtered_orig_data, data_filename)
                extlog.info("Filtered original data saved to cache file ({}).".format(data_filename))
            except Exception as e:
                extlog.error("Error while writing pickle data from file '{}' log: ({}) {}".format(
                             file_label, type(e), e.__str__()))

    @extlog.logme
    def update_data(self, data=None, end_stamp=None, file_label=None):
        if data is not None:
            self.apply_new_data(data, file_label)
            self._history_file_label = file_label

            if self._zigzag_enabled is True:
                self._zigzag_handler = ZigZagHandler(in_data=self.orig_data)

            self.reset_data()

        if self._orig_data is None:
            extlog.error("No data to update.")
            return

        self._cur_index = end_stamp-1 if end_stamp is not None else len(self.orig_data)-1
        ranged_x_values, ranged_data = self.get_processed_range(end_stamp)

        filtered_data = MathExt.hodrick_prescott_filter(ranged_data, self._la)
        self.calc_prediction(filtered_data, ranged_x_values)

        self.set_plot_data(filtered_data, data_range_list=list(ranged_x_values))

    def calc_prediction(self, filtered_data, ranged_x_values):
        back = self.extra_size+1 if self.extra_size > 0 else 1
        cur_orig_val = self._orig_data[self._cur_index]

        # x0, x1 = ranged_x_values[-(back+1)], ranged_x_values[-back]
        y0, y1 = filtered_data[-(back+1)], filtered_data[-back]

        # orig_from_filter_dist = y1-cur_orig_val
        # y0 -= orig_from_filter_dist
        # y1 -= orig_from_filter_dist
        # a = (y1 - y0) / (x1 - x0)
        # b = y1 - a * x1

        if y1-y0 > 0:
            self._predict_direction = Direction.UP
        else:
            self._predict_direction = Direction.DOWN

        # # Move line (Include error area)
        # # maxdiff = self._orig_data.max()-self._orig_data.min()
        # min, max = self.min_max_y_ext
        # width = 0.0005*(max-min)
        # b += -np.sign(a) * width/math.cos(math.atan(a))

        # self._predict_line = a, b
        self._predict_line = 0, cur_orig_val

    @extlog.logme
    def get_processed_range(self, end_stamp):
        data_range = self.construct_data_range(end_stamp)
        (start, end) = get_range_bounds(data_range)
        ranged_data = np.array(self._orig_data[start: end])
        ranged_x_values = np.array(self._x_values[start: end])
        return ranged_x_values, ranged_data

    @extlog.logme
    def calc_tangent_at_point(self, x):
        if x == 0:
            x = 1
        return self._filtered_orig_data[x] - self._filtered_orig_data[x-1]

    @extlog.logme
    def set_already_played(self, is_already_played):
        self._already_played = is_already_played

    @extlog.logme
    def log_filter_error(self, data_range_list, filtered_data):
        # Write history only for filters (+)
        # Not write history when not playing (first render) (+)

        if self._already_played:
            history_pos_from_back = 1 + self._extra_size
            cur_x_val = data_range_list[-history_pos_from_back]

            history_pos = self._cur_index
            x_value = self._x_values[history_pos]
            filtered_pos = len(filtered_data) - history_pos_from_back

            cur_filtered_val = filtered_data[filtered_pos]
            self._history_data[x_value] = cur_filtered_val

            # if history_place != filtered_pos:
            #     extlog.info("filtered_pos(%d) != history_place(%d) -- AAAAA" % (filtered_pos, history_place))

            min_max = self.min_max_y_ext

            # self._detail_history_data[FileColumns.x].append(history_pos)
            self._detail_history_data[FileColumns.x_time].append(data_range_list[filtered_pos])

            # self._detail_history_data[FileColumns.tg_val].append(self.calc_tangent_at_point(history_place))

            self._detail_history_data[FileColumns.norm_orig_val].append(
                MathExt.normalize(self._orig_data[history_pos], min_max)
            )

            self._detail_history_data[FileColumns.norm_filtered_orig_val].append(
                MathExt.normalize(self._filtered_orig_data[history_pos], min_max)
            )

            self._detail_history_data[FileColumns.norm_filtered_last_val].append(
                MathExt.normalize(cur_filtered_val, min_max)
            )

            self._detail_history_data[FileColumns.norm_error_val].append(  # Normalized absolute error
                math.fabs(self._detail_history_data[FileColumns.norm_filtered_orig_val][-1]
                          - self._detail_history_data[FileColumns.norm_filtered_last_val][-1])
            )

            self._detail_history_data[FileColumns.count_predicted].append(self.count_predicted(history_pos))

            # try:
            #     self._detail_history_data[FileColumns.zigzag_tg_val].append(
            #         self._zigzag_handler.zigzag_tangents_data[history_place]
            #     )
            # except Exception as e:
            #     extlog.error("Error while taking zigzag tangent value: " + e.__str__())
            #     self._detail_history_data[FileColumns.zigzag_tg_val].append(
            #         self._detail_history_data[FileColumns.zigzag_tg_val][-1]  # Dummy value
            #     )

            # if len(self._detail_history_data[FileColumns.x_time]) in [ 1000, 10000, 100000, 200000 ]:
            #     self.write_history_to_file(None)

    @extlog.logme
    def count_predicted(self, from_index):
        n = len(self.orig_data)

        x = 1
        val = 0
        while x < 1000:
            i = from_index + x
            if i >= n:
                break
            if self.on_the_right_side(self._x_values[i], self.orig_data[i]):
                val += 1/x
            x += 1
        return val

        # i = from_index + 1
        # while i < n and self.on_the_right_side(self._x_values[i], self.orig_data[i]) and i < from_index + 100:
        #     i += 1
        # return i - from_index - 1

    @extlog.logme
    def on_the_right_side(self, x0, y0):
        a, b = self._predict_line
        # y = a*x0+b
        # if a > 0 and y < y0:
        #     return True
        # elif a < 0 and y > y0:
        #     return True
        if self._predict_direction is Direction.UP and y0 > b:
            return True
        elif self._predict_direction is Direction.DOWN and y0 < b:
            return True

        return False

    @extlog.logme
    def log_segments_error(self, data_filename):
        pass


class DPHHPWithExtra(DPHHodrickPrescott):
    _default_extra_size = 100
    name_suffix = "HPExtra(?)"

    _extra_size = _default_extra_size

    @extlog.logme
    def __init__(self, plot_widget, la, color):
        super(DPHHPWithExtra, self).__init__(plot_widget, la, color)

    @extlog.logme
    def set_extra_size(self, extra_size):
        self._extra_size = extra_size

    @extlog.logme
    def get_extra_size(self):
        return self._extra_size

    @extlog.logme
    def get_processed_range(self, end_stamp):
        extra_size = self._extra_size
        data_range = self.construct_data_range(end_stamp)
        extended_data = self.ext_function(self._orig_data, data_range, extra_size)
        data_range = extend_range_to_right(data_range, extra_size)
        (start, end) = get_range_bounds(data_range)
        ranged_x_values = np.array(self._x_values[start: end])
        if len(ranged_x_values) < end-start:
            ranged_x_values = np.array(extend_time_range_to_right(self._x_values[start: end], extra_size, self._timeframe))
        return ranged_x_values, extended_data

    @extlog.logme
    def ext_function(self, source_data, data_range, extra_size):
        pass


class DPHHPStraight(DPHHPWithExtra):
    name_suffix = "HPStraight"

    @extlog.logme
    def __init__(self, plot_widget, la=None, color=None):
        super(DPHHPStraight, self).__init__(plot_widget, la, color)

    @extlog.logme
    def ext_function(self, source_data, data_range, extra_size):
        extlog.debug("Extending straightly")
        self._extra_size = extra_size
        (start, end) = get_range_bounds(data_range)
        extended_data = source_data[start: end]

        extended_data = np.concatenate((extended_data, np.repeat(extended_data[-1], self._extra_size)))
        return extended_data


class DPHHPTangent(DPHHPWithExtra):
    _approximation_deep = 5
    name_suffix = "HPTangent"

    @extlog.logme
    def __init__(self, plot_widget, la=None, color=None, approx_deep=5, name="HPTangent"):
        super(DPHHPTangent, self).__init__(plot_widget, la, color)
        self._approximation_deep = approx_deep
        self.name_suffix = name

    @extlog.logme
    def ext_function(self, source_data, data_range, extra_size):
        extlog.debug("Extending tangent")
        self._extra_size = extra_size
        (start, end) = get_range_bounds(data_range)
        extended_data = source_data[start: end]

        ext_data_len = len(extended_data)
        if ext_data_len < self._approximation_deep:
            extlog.error("Not enough stamps for tangent extending.")
            return extended_data

        xrange = range(ext_data_len - self._approximation_deep + 1, ext_data_len + 1)
        yrange = extended_data[-self._approximation_deep:]
        a, b = MathExt.calc_linear_trend(xrange, yrange)

        extended_data = np.concatenate((extended_data, np.repeat(0, extra_size)))
        for x in range(ext_data_len, ext_data_len + extra_size):
            extended_data[x] = a*x + b

        return extended_data


class DPHHPMiddleAngle(DPHHPWithExtra):
    name_suffix = "HPMiddleAngle"
    _approximation_deep = 5

    @extlog.logme
    def __init__(self, plot_widget, la=None, color=None):
        super(DPHHPMiddleAngle, self).__init__(plot_widget, la, color)

    @extlog.logme
    def ext_function(self, source_data, data_range, extra_size):
        extlog.debug("Extending with middle angle")
        self._extra_size = extra_size
        (start, end) = get_range_bounds(data_range)
        extended_data = source_data[start: end]

        ext_data_len = len(extended_data)
        if ext_data_len < self._approximation_deep:
            extlog.error("Not enough stamps for middle angle extending.")
            return extended_data

        xrange = range(ext_data_len - self._approximation_deep + 1, ext_data_len + 1)
        yrange = extended_data[-self._approximation_deep:]
        a, b = MathExt.calc_linear_trend(xrange, yrange)

        x0 = ext_data_len
        y0 = a*x0+b

        extended_data = np.concatenate((extended_data, np.repeat(0, extra_size)))
        for x in range(ext_data_len, ext_data_len + extra_size):
            extended_data[x] = (a/2)*(x-x0) + y0

        return extended_data


class DPHHPSpecular(DPHHPWithExtra):
    name_suffix = "HPSpecular"

    @extlog.logme
    def __init__(self, plot_widget, la=None, color=None):
        super(DPHHPSpecular, self).__init__(plot_widget, la, color)

    @extlog.logme
    def ext_function(self, source_data, data_range, extra_size):
        extlog.debug("Extending specularly")
        self._extra_size = extra_size
        (start, end) = get_range_bounds(data_range)
        extended_data = source_data[start: end]

        for i in range(self._extra_size):
            extended_data.append(extended_data[-i*2-1])

        return extended_data

