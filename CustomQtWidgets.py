from PyQt5.QtCore import pyqtSignal, QPointF, Qt
from PyQt5.QtWidgets import QMainWindow
from pyqtgraph import ViewBox

from ext_logging import extlog


class MainWindow(QMainWindow):

    key_pressed = pyqtSignal(int)

    def __init__(self, **kwargs):
        super(MainWindow, self).__init__(**kwargs)

    def keyPressEvent(self, ev):
        super(MainWindow, self).keyPressEvent(ev)

        if ev.key() in [Qt.Key_A, Qt.Key_D, Qt.Key_Left, Qt.Key_Right]:
            self.key_pressed.emit(ev.key())


class ClickableViewBox(ViewBox):
    click_here = pyqtSignal(QPointF, int)
    drag_here = pyqtSignal(QPointF)

    # Crutch here!
    event_count = 0

    def __init__(self, parent=None):
        super(ClickableViewBox, self).__init__(parent)

    def mouseClickEvent(self, ev):
        super(ClickableViewBox, self).mouseClickEvent(ev)

        if ev.button() == Qt.LeftButton or ev.button() == Qt.MiddleButton:
            # Somewhy this event comes three times
            self.event_count += 1
            if self.event_count == 3:
                self.event_count = 0
            if self.event_count != 1:
                return

            point = self.mapSceneToView(ev._scenePos)
            extlog.debug("Emmit click at {0}".format(point))
            self.click_here.emit(point, ev.button())
        else:
            ev.ignore()

    # def mouseDragEvent(self, ev, *args, **kwargs):
    #     super(ClickableViewBox, self).mouseDragEvent(ev, *args, **kwargs)
    #
    #     if ev.button() == Qt.LeftButton:
    #         point = self.mapSceneToView(ev._scenePos)
    #         self.drag_here.emit(point)
