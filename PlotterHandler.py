import traceback

import sys
from enum import Enum

from PyQt5.QtCore import QObject, QTimer, pyqtSlot, pyqtSignal, QPointF, Qt
from DataPlot import *
from pyqtgraph import ViewBox, LinearRegionItem, mkBrush


class Player(QTimer):
    # Constants
    second = 1000
    start_step = 100
    default_speed = 9

    _current_step = start_step
    _max_len = None
    _is_first_tick = True

    new_range_signal = pyqtSignal(int)
    need_move_signal = pyqtSignal()
    progress_changed = pyqtSignal(float)
    need_reset_history = pyqtSignal()
    first_tick_changed = pyqtSignal(bool)

    @extlog.logme
    def __init__(self, speed=default_speed, parent=None):
        super(Player, self).__init__(parent)
        self.set_speed(speed)
        self.timeout.connect(self.need_next_step)

    @extlog.logme
    @property
    def is_first_tick(self):
        return self._is_first_tick

    @extlog.logme
    def set_first_tick(self, is_first_tick):
        self._is_first_tick = is_first_tick
        self.first_tick_changed.emit(self._is_first_tick)

    @extlog.logme
    def get_cur_step(self):
        return self._current_step

    @extlog.logme
    def start(self, max_len):
        if self._is_first_tick:
            self.need_reset_history.emit()
            self.set_first_tick(False)
        self.set_max_len(max_len)
        self.need_next_step()  # Shoot instant
        super().start()

    def set_max_len(self, len):
        self._max_len = len

    @extlog.logme
    def emit_next_step(self):
        if not self._max_len or self._current_step > self._max_len:
            extlog.info('Step out of range. Stopping timer.')
            self.stop()
            return

        self.new_range_signal.emit(self._current_step)
        self.need_move_signal.emit()
        self.progress_changed.emit(100*self._current_step/self._max_len)

    @pyqtSlot()
    @extlog.logme
    def need_next_step(self):
        self._current_step += 1
        self.emit_next_step()

    @pyqtSlot()
    @extlog.logme
    def reset(self):
        self._current_step = Player.start_step
        self.set_first_tick(True)
        self.need_reset_history.emit()
        self.emit_next_step()

    @extlog.logme
    def set_speed(self, speed):
        interval = Player.second/pow(1.5, speed-3)
        extlog.info('Set player timer interval to %d' % interval)
        self.setInterval(interval)


class SelectionMode(Enum):
    UP = 1
    DOWN = -1
    HORIZONTAL = 0
    NONE = None

    def __str__(self):
        s = ''
        if self.value == self.UP.value:
            s = 'UP'
        elif self.value == self.DOWN.value:
            s = 'DOWN'
        elif self.value == self.HORIZONTAL.value:
            s = 'HORIZONTAL'
        elif self.value == self.NONE.value:
            s = 'NONE'

        return 'Selection ' + s


class PlotterHandler(QObject):
    # Handle operations regarding plotter
    # Common operations regarding plot view

    default_view_radius = 10

    _view_radius = default_view_radius

    _plotter = None
    _data_plot_handlers = []
    _drawing_enabled = True
    _automove_view_enabled = False

    # Variable stores mode which uses for now to select regions of graphic
    _selection_mode = SelectionMode.NONE
    _selection_segments_items = {
        SelectionMode.UP: [],
        SelectionMode.HORIZONTAL: [],
        SelectionMode.DOWN: []
    }
    # _selection_segment_items = {}
    _current_segment = None

    @extlog.logme
    def __init__(self, plotter, parent=None):
        super().__init__(parent=parent)
        self._plotter = plotter
        self._data_plot_handlers.append(DataPlotHandler(self._plotter))
        self._data_plot_handlers.append(DPHHodrickPrescott(self._plotter))
        self._data_plot_handlers.append(DPHHPStraight(self._plotter))
        self._data_plot_handlers.append(DPHHPTangent(self._plotter))
        self._data_plot_handlers.append(DPHHPMiddleAngle(self._plotter))
        # self._data_plot_handlers.append(DPHHPSpecular(self._plotter))

        self.configure_plotter(self._plotter)

    @extlog.logme
    @property
    def max_x(self):
        if len(self._data_plot_handlers) == 0:
            return 0
        # TODO: Do it another way
        return self._data_plot_handlers[0].max_x

    @extlog.logme
    def get_auto_x_view_range(self):
        last_stamp = self.max_x
        xmin = last_stamp - self._view_radius
        xmax = last_stamp + self._view_radius

        extlog.debug("New view range is (%d, %d)" % (xmin, xmax))
        return xmin, xmax

    @extlog.logme
    def get_last_orig_stamp(self):
        # Get value from first plot handler
        last_steps = [len(self._data_plot_handlers[0].orig_data)]
        return min(last_steps)

    @extlog.logme
    def set_automove_view(self, value):
        self._automove_view_enabled = value
        if self._automove_view_enabled:
            self.adjust_view_box()

    @pyqtSlot(int)
    @extlog.logme
    def set_view_radius(self, view_radius):
        self._view_radius = view_radius
        self.adjust_view_box()

    @pyqtSlot()
    @extlog.logme
    def adjust_view_box(self):
        if not self._drawing_enabled:
            return
        view_box = self._plotter.getViewBox()
        view_box.setRange(xRange=self.get_auto_x_view_range(), disableAutoRange=False)
        PlotterHandler.enable_adapt_to_y(view_box)

    @extlog.logme
    def move_view_box(self, part=10):
        if not self._drawing_enabled:
            return
        part /= 100.0
        view_box = self._plotter.getViewBox()
        cur_range = view_box.viewRange()[0]
        step = (cur_range[1]-cur_range[0])*part
        view_box.setRange(xRange=[x+step for x in cur_range], padding=0)


    @pyqtSlot()
    @extlog.logme
    def slot_enable_adapt_to_y(self):
        view_box = self._plotter.getViewBox()
        PlotterHandler.enable_adapt_to_y(view_box)

    @staticmethod
    @extlog.logme
    def get_brush_from_mode(mode):
        transparent = 70
        if mode == SelectionMode.UP:
            return mkBrush(0,255,0,transparent)
        elif mode == SelectionMode.HORIZONTAL:
            return mkBrush(255,255,0,transparent)
        elif mode == SelectionMode.DOWN:
            return mkBrush(255,0,0,transparent)
        else:
            return mkBrush()

    @extlog.logme
    def add_selection_segment_manual(self, x):
        if self._selection_mode == SelectionMode.NONE:
            return
        section_item = LinearRegionItem([x, x+5])
        section_item.setBrush(PlotterHandler.get_brush_from_mode(self._selection_mode))
        self._plotter.addItem(section_item)
        self._selection_segments_items[self._selection_mode].append(section_item)

    @extlog.logme
    def add_selection_segment(self, mode, seg):
        section_item = LinearRegionItem([seg[0], seg[1]])
        section_item.setBrush(PlotterHandler.get_brush_from_mode(mode))
        self._plotter.addItem(section_item)
        self._selection_segments_items[mode].append(section_item)

    @extlog.logme
    def remove_selection_segment(self, x):
        for mode, segments in self._selection_segments_items.items():
            for i, seg in enumerate(segments):
                region = seg.getRegion()
                if region[0] < x < region[1]:
                    self._plotter.removeItem(seg)
                    self._selection_segments_items[mode].pop(i)
                    break

    @extlog.logme
    def clear_selection_segments(self):
        for segments in self._selection_segments_items.values():
            for seg in segments:
                self._plotter.removeItem(seg)
            segments.clear()

    @pyqtSlot(QPointF, int)
    @extlog.logme
    def click_here(self, point, button):
        extlog.debug("Catched click to {}".format(point))
        if button == Qt.LeftButton:
            self.add_selection_segment_manual(point.x())
        else:
            self.remove_selection_segment(point.x())

    @staticmethod
    @extlog.logme
    def convert_segment_items_to_segments(segment_items):
        segments = {}
        for k, v in segment_items.items():
            segments[k] = [(int(seg.getRegion()[0]), int(seg.getRegion()[1])) for seg in v]
        return segments

    @pyqtSlot()
    @extlog.logme
    def save_segments(self, data_filename):
        segments = PlotterHandler.convert_segment_items_to_segments(self._selection_segments_items)
        segments_file = data_filename + '.seg'

        from FileHandler import write_structure_to_file
        write_structure_to_file(segments, segments_file)

    @extlog.logme
    def load_segments(self, data_filename):
        segments_file = data_filename + '.seg'

        from FileHandler import read_structure_from_file
        segments = read_structure_from_file(segments_file)

        self.clear_selection_segments()
        for k, v in segments.items():
            for seg in v:
                self.add_selection_segment(k, seg)


    @staticmethod
    @extlog.logme
    def enable_adapt_to_y(view_box):
        view_box.enableAutoRange(axis=ViewBox.YAxis)
        view_box.setAutoVisible(y=True)

    @staticmethod
    @extlog.logme
    def configure_plotter(plotter):
        # plotter.setClipToView(True)
        plotter.hideButtons()
        plotter.showGrid(True, True, 0.6)
        view_box = plotter.getViewBox()
        view_box.disableAutoRange(axis=ViewBox.XAxis)
        view_box.setMouseEnabled(x=True, y=False)
        PlotterHandler.enable_adapt_to_y(view_box)
        # setAspectLocked(lock=True, ratio=1)

    @extlog.logme
    def set_selection_mode(self, mode=SelectionMode.NONE):
        self._selection_mode = mode
        extlog.info("Selection mode changed to {}".format(mode))

        # view_box = self._plotter.getViewBox()
        # if self._selection_mode != SelectionMode.NONE:
        #     view_box.setMouseEnabled(x=False, y=False)
        # else:
        #     view_box.setMouseEnabled(x=True, y=False)

    @extlog.logme
    def set_first_tick(self, is_first_tick):
        for dph in self._data_plot_handlers:
            if not isinstance(dph, DPHHodrickPrescott):
                continue
            dph.set_already_played(not is_first_tick)

    @extlog.logme
    def set_range(self, data_range):
        self.set_series(data_range=data_range)

    @extlog.logme
    def set_series(self, data=None, data_range=None, file_label=None):
        for dph in self._data_plot_handlers:
            dph.update_data(data=data, end_stamp=data_range, file_label=file_label)

    @extlog.logme
    def set_timeframe(self, timeframe):
        for dph in self._data_plot_handlers:
            dph.set_timeframe(timeframe)

    @extlog.logme
    @property
    def plotter(self):
        return self._plotter

    @extlog.logme
    def get_drawing_enabled(self):
        return self._drawing_enabled

    @extlog.logme
    def toggle_drawing_enabled(self):
        self._drawing_enabled = not self._drawing_enabled

        self._plotter.setEnabled(self._drawing_enabled)
        self.set_to_all_visible(self._drawing_enabled)
        return self._drawing_enabled

    @extlog.logme
    def set_to_all_visible(self, is_visible):
        for dph in self._data_plot_handlers:
            dph.set_need_rendering(is_visible)

    @extlog.logme
    def set_to_all_extra_size(self, extra_size):
        for dph in self._data_plot_handlers:
            if hasattr(dph, 'set_extra_size'):
                dph.set_extra_size(extra_size)

    @extlog.logme
    def set_to_all_lambda(self, la):
        for dph in self._data_plot_handlers:
            # Skip not Hodrick-Prescott dph
            if not isinstance(dph, DPHHodrickPrescott):
                continue
            dph.set_la(la)

    @pyqtSlot(int)
    @extlog.logme
    def set_to_all_filter_deep(self, deep):
        for dph in self._data_plot_handlers:
            # Skip not Hodrick-Prescott dph
            if not isinstance(dph, DPHHodrickPrescott):
                continue
            dph.set_filter_deep(deep)

    @pyqtSlot()
    @extlog.logme
    def write_all_history(self):
        for dph in self._data_plot_handlers:
            try:
                if isinstance(dph, DPHHodrickPrescott):
                    segments = PlotterHandler.convert_segment_items_to_segments(self._selection_segments_items)
                    dph.write_history_to_file(segments)
            except Exception as e:
                traceback.print_tb(sys.exc_info()[2])
                extlog.error(str(type(e)) + ": " + e.__str__())


    @extlog.logme
    def reset_to_all_history(self):
        self.write_all_history()

        for dph in self._data_plot_handlers:
            try:
                if isinstance(dph, DPHHodrickPrescott):
                    dph.reset_history()
            except Exception as e:
                traceback.print_tb(sys.exc_info()[2])
                extlog.error(str(type(e)) + ": " + e.__str__())

    @extlog.logme
    def set_to_all_status_handler(self, status_handler):
        for dph in self._data_plot_handlers:
            dph.set_status_handler(status_handler)
