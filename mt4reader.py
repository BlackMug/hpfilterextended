from ext_logging import extlog
import struct
import logging
import datetime
import numpy as np

#http://profitraders.com/Python/hstRead.html


# Structure of value
class Value:
    def __init__(self, data_tuple):
        self.datetime = datetime.datetime.fromtimestamp(data_tuple[0])
        self.open = data_tuple[1]
        # self.low = data_tuple[2]
        # self.high = data_tuple[3]
        # self.close = data_tuple[4]
        # self.vol = data_tuple[5]
        # self.spread = data_tuple[6]
        # self.realvol = data_tuple[7]

    def __str__(self):
        return """ Value:
        Datetime = {v.datetime}
        Open = {v.open}
        Low = {v.low}
        High = {v.high}
        Close = {v.close}
        Vol = {v.vol}
        Spread = {v.spread}
        Realvol = {v.realvol}
        """.format(v=self)


class mt4reader:
    _filename = ""
    _filestream = None
    _data = []

    @staticmethod
    @extlog.logme
    def get_open_data(data):
        ret = np.array([value.datetime for value in data]), np.array([value.open for value in data])
        return ret

    @extlog.logme
    def __init__(self, filename):
        self._filename = filename
        try:
            self._filestream = open(self._filename, 'rb')
            logging.info('File %s opened successfully.' % self._filename)
        except FileExistsError:
            logging.error('File %s not exist.' % self._filename)
        except FileNotFoundError:
            logging.error('File %s not exist.' % self._filename)
        finally:
            pass

    @extlog.logme
    def __exit__(self, exc_type, exc_val, exc_tb):
        self._filestream.close()

    @extlog.logme
    def read_data(self):
        if self._filestream is None or self._filename is None:
            logging.info('mt4reader instance is not inited.')
            return -1

        self._ver, = struct.unpack('<i', self._filestream.read(4))
        logging.info('Version: %d' % self._ver)

        # Copywrite
        self._copywrite = self._filestream.read(64)
        logging.info('%s' % self._copywrite)

        # Ticker
        self._symbol = self._filestream.read(12)
        logging.info('Symbol: %s' % self._symbol)

        self._period, self._digits, self._timesign, self._last_sync = struct.unpack('<iiii', self._filestream.read(16))
        logging.info('Timeframe: %d' % int(self._period))
        logging.info('Digits: %d' % int(self._digits))
        self._timesign_dt = datetime.datetime.fromtimestamp(self._timesign)
        logging.info('Timesign: %s' % str(self._timesign_dt))
        self._last_sync_dt = datetime.datetime.fromtimestamp(self._last_sync) if self._last_sync else None
        logging.info('Last sync: %s' % str(self._last_sync_dt))

        # Skip reserved or useless data
        self._filestream.read(52)

        data_len = 60
        self._data.clear()
        while data_len:
            value = self._filestream.read(data_len)
            # logging.info('Readed %d bytes as one value.' % data_len)
            if data_len != len(value):
                data_len = len(value)
                break
            value_tuple = struct.unpack('<QddddQLQ', value)
            self._data.append(Value(value_tuple))

        if data_len != 0:
            logging.error('Last value was read not fully: %d/60' % data_len)
        logging.info('End of reading data. Data read: {}'.format(len(self._data)))

        return 0

    @property
    def get_data(self):
        return self._data

    @property
    def get_timeframe(self):
        return self._period * 60  # since timestamp is in seconds
