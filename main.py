from ext_logging import extlog
import sys

from pyqtgraph.Qt import QtGui
from MainClass import MainClass

profiling = False


def main():
    extlog.init_log()
    extlog.set_level(extlog.INFO)

    app = QtGui.QApplication(sys.argv)
    # Creating object of MainClass required because of garbage collector
    main_class = MainClass("Forms/Plotter_main.ui", app)

    if not profiling:
        sys.exit(app.exec_())
    else:
        app.exec_()


if __name__ == '__main__':

    if not profiling:
        main()
    else:
        from cProfile import Profile

        profiler = Profile()
        profiler.run("main()")
        from pyprof2calltree import convert, visualize

        visualize(profiler.getstats())
        convert(profiler.getstats(), 'profiling_results.kgrind')
