from ext_logging import extlog
import numpy as np


@extlog.logme
def calc_linear_trend(xrange, yrange):
    assert (len(xrange) == len(yrange))
    n = len(xrange)

    xsum = sum(xrange)
    ysum = sum(yrange)
    x2sum = sum([x * x for x in xrange])
    xysum = sum([xrange[i] * yrange[i] for i in range(n)])

    a = (n * xysum - xsum * ysum) / \
        (n * x2sum - xsum * xsum)
    b = (ysum - a * xsum) / n

    return a, b


@extlog.logme
def calc_correlation(X, Y):

    n = len(X)
    if len(Y) != n:
        extlog.error("Error while calculating correlation. Arrays have different size.")
        return []

    sumx = sumy = 0
    for i in range(n):
        sumx += X[i]
        sumy += Y[i]

    MX = sumx / n
    MY = sumy / n

    DX = DY = 0
    M_mul_dif = 0
    for i in range(n):
        tmpx = X[i]-MX
        tmpy = Y[i]-MY

        DX += tmpx**2
        DY += tmpy**2

        M_mul_dif += tmpx*tmpy
    DX /= n
    DY /= n
    M_mul_dif /= n

    sigX = DX ** 0.5
    sigY = DY ** 0.5

    return M_mul_dif / (sigX * sigY)


@extlog.logme
def normalize(value, min_max):
    min_val, max_val = min_max
    return (value-min_val)/(max_val-min_val)


@extlog.logme
def hodrick_prescott_filter_(data, la):

    n = data.size
    F = np.zeros((n, n), dtype=np.float)

    np.fill_diagonal(F[2:,:], 1)
    np.fill_diagonal(F[1:,:], -4)
    np.fill_diagonal(F[::], 6)
    np.fill_diagonal(F[:,2:], 1)
    np.fill_diagonal(F[:,1:], -4)

    F[0,0] = F[-1,-1] = 1
    F[1,1] = F[-2,-2] = 5
    F[0,1] = F[1,0] = F[-2,-1] = F[-1,-2] = -2

    F *= la
    F += np.eye(n)

    F = np.linalg.inv(F)

    ans = np.matmul(F, data)

    return ans


@extlog.logme
def hodrick_prescott_filter(data, la):
    # Changes input data variable
    extlog.debug('Data size: %d, Lambda value:%d' % (len(data), la))

    curve = data
    n = len(curve)

    a = [0.0] * n
    b = [0.0] * n
    c = [0.0] * n

    for i in range(0, n):
        a[i] = 6 * la + 1
        b[i] = -4 * la
        c[i] = la

    a[0] = a[-1] = 1 + la
    a[1] = a[-2] = 5 * la + 1
    b[1] = b[-1] = -2 * la
    b[0] = c[0] = c[1] = 0
    c[-1] = la

    h1 = h2 = h3 = h4 = h5 = hh2 = hh3 = hh5 = 0
    for i in reversed(range(0, n)):
        z = a[i] - h4 * h1 - hh5 * hh2
        hb = b[i]
        hh1 = h1
        h1 = (hb - h4 * h2) / z
        b[i] = h1
        hc = c[i]
        hh2 = h2
        h2 = hc / z
        c[i] = h2
        a[i] = (curve[i] - hh3 * hh5 - h3 * h4) / z
        hh3 = h3
        h3 = a[i]
        h4 = hb - h5 * hh1
        hh5 = h5
        h5 = hc

    h2 = 0
    h1 = a[0]
    curve[0] = h1

    for i in range(1, n):
        curve[i] = a[i] - b[i] * h1 - c[i] * h2
        h2 = h1
        h1 = curve[i]

    return curve


@extlog.logme
def zigzag_simple(data, noise_height):

    # Since this value is last significant symbol of rounding input data values
    POINT = 0.0001
    noise_height *= POINT

    # Directions enum
    UP = 1
    DOWN = -1
    UNKNOWN = 0

    prev_max = prev_min = data[0]
    direction = UNKNOWN

    extremums = [(0, data[0])]

    for i, cur in enumerate(data):

        if direction == UP:
            if cur - prev_min > noise_height and prev_max < cur:
                prev_max = cur
                extremums[-1] = (i, cur)
            elif prev_max - cur > noise_height:
                direction = DOWN
                prev_min = cur
                extremums.append((i, cur))

        elif direction == DOWN:
            if prev_max - cur > noise_height and prev_min > cur:
                prev_min = cur
                extremums[-1] = (i, cur)
            elif cur - prev_min > noise_height:
                direction = UP
                prev_max = cur
                extremums.append((i, cur))

        else:
            diff = data[0]-cur
            if diff > noise_height:
                direction = UP
            elif (-diff) > noise_height:
                direction = DOWN

    return {i: v for i, v in extremums if v != 0}


@extlog.logme
def zigzag(depth, deviation, backstep, data):

    # Since this value is last significant symbol of rounding input data values
    POINT = 0.0001

    n = len(data)
    if n < depth or backstep >= depth:
        return {}

    # limit = n-depth
    low_buf = [0]*n
    high_buf = [0]*n
    common_buf = [0]*n

    lastlow = None
    lasthigh = None
    for i in range(depth-1, n):

        # Find minimum
        extr = min(data[i-depth+1 : i+1])
        if extr == lastlow:
            extr = None
        else:
            lastlow = extr

            if data[i]-extr > deviation*POINT:
                extr = None
            else:
                for back in range(1,backstep):
                    pos = i-back
                    if low_buf[pos] != 0 and low_buf[pos] > extr:
                        low_buf[pos] = 0

        if data[i] == extr:
            low_buf[i] = extr
        else:
            low_buf[i] = 0

        # Find maximum
        extr = max(data[i-depth+1 : i+1])
        if extr == lasthigh:
            extr = None
        else:
            lasthigh = extr

            if extr - data[i] > deviation*POINT:
                extr = None
            else:
                for back in range(1, backstep):
                    pos = i-back
                    if high_buf[pos] != 0 and high_buf[pos] < extr:
                        high_buf[pos] = 0

        if data[i] == extr:
            high_buf[i] = extr
        else:
            high_buf[i] = 0

    whatlookfor = 0
    lastlow = 0
    lasthigh = 0

    HIGH=1
    LOW=-1
    ALL=0

    for i in range(depth-1, n):
        if whatlookfor == ALL:

            if lastlow == 0 and lasthigh == 0:
                if high_buf[i] != 0:
                    lasthigh = data[i]
                    lasthighpos = i
                    whatlookfor = LOW
                    common_buf[i] = lasthigh

                if low_buf[i] != 0:
                    lastlow = data[i]
                    lastlowpos = i
                    whatlookfor = HIGH
                    common_buf[i] = lastlow

        elif whatlookfor == HIGH:

            if low_buf[i] != 0 and low_buf[i] < lastlow and high_buf[i] == 0:
                common_buf[lastlowpos] = 0
                lastlowpos=i
                lastlow = low_buf[i]
                common_buf[i] = lastlow

            if high_buf[i] != 0 and low_buf[i] == 0:
                lasthigh = high_buf[i]
                lasthighpos = i
                common_buf[i] = lasthigh
                whatlookfor = -1

        elif whatlookfor == LOW:

            if high_buf[i] != 0 and high_buf[i]>lasthigh and low_buf[i] == 0:
                common_buf[lasthighpos] = 0
                lasthighpos = i
                lasthigh = high_buf[i]
                common_buf[i] = lasthigh

            if low_buf[i] != 0 and high_buf[i] == 0:
                lastlow = low_buf[i]
                lastlowpos = i
                common_buf[i] = lastlow
                whatlookfor = 1

    return {i: v for i, v in enumerate(common_buf) if v != 0}
